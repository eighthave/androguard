androguard (3.4.0~a1-17) unstable; urgency=medium

  * fix Python 3.12 syntax error (Closes: #1061593)
  * fix ResParserError: reserved must be zero!

 -- Hans-Christoph Steiner <hans@eds.org>  Tue, 04 Mar 2025 09:05:08 +0100

androguard (3.4.0~a1-16) unstable; urgency=medium

  * Team upload.
  * Add dep on python3-colorama (Closes: #1091936)

 -- Alexandre Detiste <tchet@debian.org>  Wed, 08 Jan 2025 10:28:09 +0100

androguard (3.4.0~a1-15) unstable; urgency=medium

  * fix cg command with newer networkx (Closes: #1072592, #1073403)

 -- Hans-Christoph Steiner <hans@eds.org>  Mon, 09 Sep 2024 21:26:54 +0200

androguard (3.4.0~a1-14) unstable; urgency=medium

  * Add updated res0/res1 patch from upstream

 -- Hans-Christoph Steiner <hans@eds.org>  Fri, 15 Mar 2024 12:33:45 +0100

androguard (3.4.0~a1-13) unstable; urgency=medium

  * Re-enable complete test suite.

 -- Hans-Christoph Steiner <hans@eds.org>  Wed, 13 Mar 2024 16:57:30 +0100

androguard (3.4.0~a1-12) unstable; urgency=medium

  * fix crash with bad logging variable

 -- Hans-Christoph Steiner <hans@eds.org>  Wed, 13 Mar 2024 14:26:25 +0100

androguard (3.4.0~a1-11) unstable; urgency=medium

  * Replace res0 patch with better patch from upstream

 -- Hans-Christoph Steiner <hans@eds.org>  Wed, 13 Mar 2024 13:52:06 +0100

androguard (3.4.0~a1-10) unstable; urgency=medium

  * Team upload.
  * Skip autopkgtest on s390x (See: #890593)

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 26 Jan 2024 08:56:37 +0100

androguard (3.4.0~a1-9) unstable; urgency=medium

  * Team upload.
  * Fix failing build (Closes: #1058146)
  * Use pybuild.testfiles

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 25 Jan 2024 15:11:06 +0100

androguard (3.4.0~a1-8) unstable; urgency=medium

  * Team upload.
  * remove useless, deprecated, build-dep python3-future

 -- Alexandre Detiste <tchet@debian.org>  Thu, 04 Jan 2024 16:30:02 +0100

androguard (3.4.0~a1-7) unstable; urgency=medium

  * patch to remove unneeded "res0 must be always zero" error

 -- Hans-Christoph Steiner <hans@eds.org>  Wed, 12 Jul 2023 11:33:59 +0200

androguard (3.4.0~a1-6) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on python3-cryptography and
      python3-mock.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 16 Oct 2022 23:41:40 +0100

androguard (3.4.0~a1-5) unstable; urgency=medium

  * Team upload.
  * Use pytest instead of nose. (Closes: #1018306)
  * Add debian/tests/arsc.

 -- Emanuele Rocca <ema@debian.org>  Mon, 26 Sep 2022 20:23:24 +0200

androguard (3.4.0~a1-4) unstable; urgency=medium

  * Team upload.
  * Fix pyyaml dependency
  * Drop isolation-container in autopkgtests
  * Bump policy version (no changes)
  * Update d/copyright

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 05 Jul 2022 09:01:28 +0200

androguard (3.4.0~a1-3) unstable; urgency=medium

  * Team upload.
  * Add patch for new NetworkX (Closes: #1011713)

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 04 Jul 2022 10:43:29 +0200

androguard (3.4.0~a1-2) unstable; urgency=medium

  * Team upload.
  * Add patch for oscrypto
  * Update packaging

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 12 Apr 2022 12:27:09 +0200

androguard (3.4.0~a1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Hans-Christoph Steiner ]
  * New upstream version 3.4.0~a1

 -- Hans-Christoph Steiner <hans@eds.org>  Fri, 20 Nov 2020 11:54:20 +0100

androguard (3.3.5-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Diego M. Rodriguez ]
  * d/control: replace pyasn1 with asn1crypto (Closes: #951961)

  [ Emmanuel Arias ]
  * d/gbp.conf: set debian/master as default branch.
  * Rename d/.gitlab-ci.yml to salsa-ci.yml.
    - Switch image to salsa-ci recommendation.

 -- Hans-Christoph Steiner <hans@eds.org>  Mon, 02 Mar 2020 00:22:41 -0300

androguard (3.3.5-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Sandro Tosi ]
  * debian/rules
    - run unittests at build-time
  * debian/patches/networkx-2.4.patch
    - compatiblity with networkx 2.4; Closes: #945390

 -- Sandro Tosi <morph@debian.org>  Tue, 31 Dec 2019 20:20:28 -0500

androguard (3.3.5-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Hans-Christoph Steiner ]
  * New upstream version
  * audroauto was removed by upstream

 -- Hans-Christoph Steiner <hans@eds.org>  Thu, 17 Oct 2019 20:54:46 +0200

androguard (3.3.3-1) unstable; urgency=medium

  * New upstream version

 -- Hans-Christoph Steiner <hans@eds.org>  Mon, 28 Jan 2019 12:10:36 +0100

androguard (3.3.1-1) unstable; urgency=medium

  * New upstream version

 -- Hans-Christoph Steiner <hans@eds.org>  Fri, 25 Jan 2019 15:49:45 +0000

androguard (3.2.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Convert git repository from git-dpm to gbp layout

  [ Hans-Christoph Steiner ]
  * New upstream version
  * enable full test suite in autopkgtest

 -- Hans-Christoph Steiner <hans@eds.org>  Tue, 25 Sep 2018 12:41:58 +0200

androguard (3.1.0~rc2-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python3-Version field

  [ Hans-Christoph Steiner ]
  * disable tests that fail on big-endian and re-enable on those arches
  * Standards-Version: 4.1.4 no changes

 -- Hans-Christoph Steiner <hans@eds.org>  Wed, 16 May 2018 20:21:04 +0100

androguard (3.1.0~rc2-1) unstable; urgency=medium

  * New upstream version 3.1.0~rc2

 -- Hans-Christoph Steiner <hans@eds.org>  Wed, 21 Feb 2018 20:21:04 +0100

androguard (3.1.0~rc1-2) unstable; urgency=medium

  * disable big-endian arches (Closes: #890593)

 -- Hans-Christoph Steiner <hans@eds.org>  Mon, 19 Feb 2018 22:09:47 +0100

androguard (3.1.0~rc1-1) unstable; urgency=medium

  [ Hans-Christoph Steiner ]
  * New upstream versions
  * Switch from python2 to python3
  * remove .py from commands

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field

 -- Hans-Christoph Steiner <hans@eds.org>  Tue, 13 Feb 2018 10:19:36 +0100

androguard (2.0-3) unstable; urgency=medium

  [ Bhavani Shankar ]
  * upstream commit e1bd2a7 to fix compilation on non-x86 (Closes: #849647)

 -- Hans-Christoph Steiner <hans@eds.org>  Sat, 11 Mar 2017 10:17:40 +0100

androguard (2.0-2) unstable; urgency=medium

  * lintian overrides: the included binaries are test objects
  * manually include python-networkx (Closes: #823309)
  * include Vcs tags
  * Uploaders: Debian Python Modules Team
  * Standards-Version: 3.9.8, no changes
  * remove pointless README (Closes: #824978)

 -- Hans-Christoph Steiner <hans@eds.org>  Wed, 01 Jun 2016 12:08:23 +0200

androguard (2.0-1) unstable; urgency=low

  * Initial release. (Closes: #811421)

 -- Hans-Christoph Steiner <hans@eds.org>  Mon, 18 Jan 2016 20:54:01 +0100
